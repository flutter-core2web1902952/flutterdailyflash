// Create a screen that display 3 widgets in column. The image must be the first widget the next must be container of color red and third must be a container of color blue.Place all 3 widgets in a column. The image must be placed at the top center and the other 2 widgets must be placed at he bottom center of screen.

import 'package:flutter/material.dart';

class Que25 extends StatefulWidget {
  const Que25({super.key});

  @override
  State<Que25> createState() => _Que25State();
}

class _Que25State extends State<Que25> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: Colors.blue,
        title: const Text("Daily Flash"),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Image.asset(
              "assets/images/que22img1.jpeg",
            ),

            const SizedBox(
              height: 60,
            ),
            //const Spacer(),
            Container(
              width: 100,
              height: 100,
              color: Colors.red,
            ),

            Container(
              width: 100,
              height: 100,
              color: Colors.blue,
            ),
          ],
        ),
      ),
    );
  }
}
