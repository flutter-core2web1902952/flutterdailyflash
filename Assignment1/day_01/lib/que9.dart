// Create a container that will have a border. The toop right and bottom left corners of the border must be rounded . Now display the text in the container and give appropriate padding to the container.

import 'package:flutter/material.dart';

class Que9 extends StatefulWidget {
  const Que9({super.key});

  @override
  State<Que9> createState() => _Que9State();
}

class _Que9State extends State<Que9> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: const Color.fromARGB(252, 106, 59, 6),
        centerTitle: true,
        title: const Text(
          "Assignment 2",
          style: TextStyle(
            fontWeight: FontWeight.w800,
            fontSize: 15,
            color: Colors.white,
          ),
        ),
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(
            bottom: Radius.circular(30),
          ),
        ),
      ),
      body: Center(
        child: Container(
          width: 300,
          height: 100,
          padding: const EdgeInsets.all(8.0),
          decoration: BoxDecoration(
            color: const Color.fromARGB(255, 223, 129, 209),
            border: Border.all(
              color: const Color.fromARGB(255, 108, 2, 103),
            ),
            borderRadius: const BorderRadius.only(
              topLeft: Radius.circular(30),
              bottomRight: Radius.circular(
                30,
              ),
            ),
          ),
          child: const Text("Container"),
        ),
      ),
    );
  }
}
