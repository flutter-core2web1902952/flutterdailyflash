// Create a screen and then add a floating action button.In thid button, you will have to display your name and an icon which must be placed in a row.

import 'package:flutter/material.dart';

class Que18 extends StatefulWidget {
  const Que18({super.key});

  @override
  State<Que18> createState() => _Que18State();
}

class _Que18State extends State<Que18> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Container(
          width: 300,
          height: 100,
          child: FloatingActionButton(
            onPressed: () {},
            child: const Row(
              children: [
                SizedBox(
                  width: 15,
                ),
                Icon(
                  size: 50,
                  Icons.person,
                ),
                Spacer(),
                Text(
                  "Mohit",
                  style: TextStyle(
                    fontSize: 40,
                  ),
                ),
                SizedBox(
                  width: 95,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
