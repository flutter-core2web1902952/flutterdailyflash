// Create a screen and add your image in the center of screen below your image display your name in a container , give a shadow to the continaer and give a border to the container topleft and topright corners must be circular,with radius 20. Add appropraite padding to the container.

import 'package:flutter/material.dart';

class Que23 extends StatefulWidget {
  const Que23({super.key});

  @override
  State<Que23> createState() => _Que23State();
}

class _Que23State extends State<Que23> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.amber,
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset(
              width: 220,
              height: 300,
              "assets/images/profileimage.jpeg",
            ),
            const SizedBox(
              height: 20,
            ),
            Container(
              decoration: const BoxDecoration(
                color: Color.fromARGB(255, 225, 132, 10),
                boxShadow: [
                  BoxShadow(
                    offset: Offset(0, 4),
                    blurRadius: 30,
                    spreadRadius: 3,
                  ),
                ],
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(20),
                  topRight: Radius.circular(20),
                ),
              ),
              padding: const EdgeInsets.only(
                top: 12,
                left: 38,
              ),
              width: 250,
              height: 50,
              child: const Text(
                "Mohit Meghraj Bawankar",
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 16,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
