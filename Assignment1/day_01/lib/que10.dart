// Add a container with the color red and display the text "Click me! ", in the center of the container.On tapping the container, the text must change to "Container tapped" and the color of the container must change to blue,

import 'package:flutter/material.dart';

class Que10 extends StatefulWidget {
  const Que10({super.key});

  @override
  State<Que10> createState() => _Que10State();
}

class _Que10State extends State<Que10> {
  bool isToggled = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: const Color.fromRGBO(95, 63, 4, 0.996),
        title: const Text(
          "Assignment 2",
          style: TextStyle(
            fontWeight: FontWeight.w800,
            fontSize: 15,
          ),
        ),
      ),
      body: Center(
        child: GestureDetector(
          onTap: () {
            setState(() {
              isToggled = !isToggled;
            });
          },
          child: Center(
            child: Container(
              width: 300,
              height: 300,
              color: (isToggled) ? Colors.blue : Colors.red,
              child: Center(
                child: Text(
                  (isToggled) ? "Container Tapped" : "Click me!",
                  style: const TextStyle(
                    fontWeight: FontWeight.w800,
                    fontSize: 15,
                    color: Colors.white,
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
