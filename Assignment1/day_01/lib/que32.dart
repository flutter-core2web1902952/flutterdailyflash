// Create a screen that display an icon widget representing a star the size of the icon must be 40 and the color of the icon must be orange ,beside the icon place a 'Text' widget with the content "Rating: 4.5". Apply a fontsize of 25 and bold weight to the text.

import 'package:flutter/material.dart';

class Que32 extends StatefulWidget {
  const Que32({super.key});

  @override
  State<Que32> createState() => _Que32State();
}

class _Que32State extends State<Que32> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blue,
        title: const Text(
          "Daily Flash",
          style: TextStyle(
            fontWeight: FontWeight.w500,
            fontSize: 20,
            color: Colors.black,
          ),
        ),
      ),
      body: Center(
        child: Container(
          width: 250,
          height: 70,
          decoration: BoxDecoration(
            borderRadius: const BorderRadius.all(
              Radius.circular(30),
            ),
            border: Border.all(
              color: Colors.black,
            ),
          ),
          child: const Row(
            children: [
              SizedBox(
                width: 30,
              ),
              Icon(
                size: 40,
                color: Colors.orange,
                Icons.star,
              ),
              SizedBox(
                width: 10,
              ),
              Text(
                "Rating: 4.5",
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 25,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
