// Create a container in the center of the screen with size(width:300,height:300) and display an image in the center of the container . Apply appropriate padding to the container.

import 'package:flutter/material.dart';

class Que11 extends StatelessWidget {
  const Que11({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: const Color.fromRGBO(176, 81, 4, 0.988),
        title: const Text(
          "Assignment3",
          style: TextStyle(
            fontWeight: FontWeight.w800,
            fontSize: 15,
          ),
        ),
      ),
      body: Center(
        child: Container(
          width: 300,
          height: 300,
          padding: const EdgeInsets.all(9),
          color: Colors.grey,
          child: Image.asset(
            "assets/images/img5.jpg",
          ),
        ),
      ),
    );
  }
}
