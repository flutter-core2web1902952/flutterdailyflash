import 'package:flutter/material.dart';
// import 'package:day_01/que1.dart';
// import 'package:day_01/que2.dart';
// import 'package:day_01/que3.dart';
// import 'package:day_01/que4.dart';
import 'package:day_01/que35.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Que35(),
    );
  }
}
