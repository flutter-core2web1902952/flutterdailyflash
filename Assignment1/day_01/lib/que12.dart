// Create a container in the center of the screen ,now in the background of the container display an image (image can be an assest or network image). Also, display text in the center of the container.

import 'package:flutter/material.dart';

class Que12 extends StatelessWidget {
  const Que12({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Container(
          decoration: const BoxDecoration(
            image: DecorationImage(
              image: AssetImage("assets/images/img3_2.jpeg"),
            ),
          ),
          width: 500,
          height: 300,
          child: const Center(
            child: Text("Core2web"),
          ),
        ),
      ),
    );
  }
}
