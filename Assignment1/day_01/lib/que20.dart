// Create screen and a floatingactionbutton in the bottom center of the screen when button is long pressed change the color to purple.

import 'package:flutter/material.dart';

class Que20 extends StatefulWidget {
  const Que20({super.key});

  @override
  State<Que20> createState() => _Que20State();
}

class _Que20State extends State<Que20> {
  Color changedColor = Colors.orange;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: FloatingActionButton(
          backgroundColor: changedColor,
          onPressed: () {
            setState(() {
              changedColor = Colors.red;
            });
          },
          child: const Text("Click"),
        ),
      ),
    );
  }
}
