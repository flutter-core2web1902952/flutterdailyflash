// Add a floating action button on the screen and when we hover over the button the color of the button must become orange.

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class Que19 extends StatefulWidget {
  const Que19({super.key});

  @override
  State<Que19> createState() => _Que19State();
}

class _Que19State extends State<Que19> {
  bool hovered = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Container(
          width: 200,
          height: 100,
          child: FloatingActionButton(
            enableFeedback: true,
            hoverElevation: 2,
            hoverColor: Colors.orange,
            onPressed: () {
              setState(() {
                (hovered) ? Colors.orange : Colors.blue;
              });
            },
            child: const Text("Click"),
          ),
        ),
      ),
    );
  }
}
