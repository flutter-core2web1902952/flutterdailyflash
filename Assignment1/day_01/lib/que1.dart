// Create an appbar having an icon in the start,title in the middle and icon in the end.

import 'package:flutter/material.dart';

class Que1 extends StatefulWidget {
  const Que1({super.key});

  @override
  State<Que1> createState() => _Que1State();
}

class _Que1State extends State<Que1> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: const Color.fromARGB(255, 73, 32, 2),
        centerTitle: true,
        title: const Text(
          "AppBar",
          style: TextStyle(
            fontSize: 15,
            fontWeight: FontWeight.w700,
          ),
        ),
        leading: const Icon(
          Icons.menu,
        ),
        actions: const [
          Icon(
            Icons.search,
          ),
          SizedBox(
            width: 15,
          ),
        ],
      ),
    );
  }
}
