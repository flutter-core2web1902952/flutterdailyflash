//

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class Que30 extends StatefulWidget {
  const Que30({super.key});

  @override
  State<Que30> createState() => _Que30State();
}

class _Que30State extends State<Que30> {
  bool isTapped1 = false;
  bool isTapped2 = false;
  bool isTapped3 = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blue,
        title: const Text(
          "Daily Flash",
          style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 20,
          ),
        ),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            GestureDetector(
              onTap: () {
                // isTapped = true;
                setState(() {
                  isTapped1 = !isTapped1;
                });
              },
              child: Container(
                width: 200,
                height: 100,
                decoration: BoxDecoration(
                  color: (isTapped1) ? Colors.red : Colors.white,
                  border: Border.all(
                    color: Colors.black,
                  ),
                ),
              ),
            ),
            GestureDetector(
              onTap: () {
                // isTapped = true;
                setState(() {
                  isTapped2 = !isTapped2;
                });
              },
              child: Container(
                width: 200,
                height: 100,
                decoration: BoxDecoration(
                  color: (isTapped2) ? Colors.red : Colors.white,
                  border: Border.all(
                    color: Colors.black,
                  ),
                ),
              ),
            ),
            GestureDetector(
              onTap: () {
                // isTapped = true;
                setState(() {
                  isTapped3 = !isTapped3;
                });
              },
              child: Container(
                width: 200,
                height: 100,
                decoration: BoxDecoration(
                  color: (isTapped3) ? Colors.red : Colors.white,
                  border: Border.all(
                    color: Colors.black,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
