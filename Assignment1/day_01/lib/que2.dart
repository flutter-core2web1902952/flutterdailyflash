// Create an appbar having an icon in the start, and 3 icons at the end.

import 'package:flutter/material.dart';

class Que2 extends StatefulWidget {
  const Que2({super.key});

  @override
  State<Que2> createState() => _Que2State();
}

class _Que2State extends State<Que2> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: const Color.fromARGB(255, 73, 32, 2),
        leading: const Icon(Icons.menu),
        actions: const [
          Icon(Icons.search),
          SizedBox(
            width: 20,
          ),
          Icon(Icons.notifications),
          SizedBox(
            width: 20,
          ),
          Icon(Icons.send),
          SizedBox(
            width: 20,
          ),
        ],
      ),
    );
  }
}
