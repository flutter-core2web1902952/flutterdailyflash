// Create a screen in which we will display 3 container of size 100 in a column. Give color to containers. The containers must divide the free space in the main axis evenly among each other.

import 'package:flutter/material.dart';

class Que24 extends StatefulWidget {
  const Que24({super.key});

  @override
  State<Que24> createState() => _Que24State();
}

class _Que24State extends State<Que24> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blue,
        title: const Text("Daily Flash"),
      ),
      body: Center(
        child: Column(
          // crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Container(
              width: 100,
              height: 100,
              color: Colors.red,
            ),
            Container(
              width: 100,
              height: 100,
              color: Colors.yellow,
            ),
            Container(
              width: 100,
              height: 100,
              color: Colors.green,
            ),
          ],
        ),
      ),
    );
  }
}
