// Create a screen, in the center of the screen dsplay a container with rounded corner give a specific color to the container ,the container must have a shadow of color red.

import 'package:flutter/material.dart';

class Que5 extends StatefulWidget {
  const Que5({super.key});

  @override
  State<Que5> createState() => _Que5State();
}

class _Que5State extends State<Que5> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("AppBar"),
        centerTitle: true,
        backgroundColor: const Color.fromARGB(253, 102, 44, 3),
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(
            bottom: Radius.circular(30),
          ),
        ),
      ),
      body: Center(
        child: Container(
          width: 300,
          height: 300,
          decoration: const BoxDecoration(
            color: Colors.yellow,
            boxShadow: [
              BoxShadow(
                color: Colors.red,
                blurRadius: 20,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
