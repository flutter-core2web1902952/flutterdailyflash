// In the screen add a container of size(width:100,height:100).The container must have a border as displayed in the below image.Give a color to the container and border as per your choice.

import 'package:flutter/material.dart';

class Que8 extends StatefulWidget {
  const Que8({super.key});

  @override
  State<Que8> createState() => _Que8State();
}

class _Que8State extends State<Que8> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: const Color.fromRGBO(95, 63, 4, 0.996),
        title: const Text(
          "Assignment 2",
          style: TextStyle(
            fontWeight: FontWeight.w800,
            fontSize: 15,
          ),
        ),
      ),
      body: Center(
        child: Container(
          width: 100,
          height: 100,
          decoration: BoxDecoration(
            color: const Color.fromARGB(255, 214, 138, 227),
            border: Border.all(
              color: const Color.fromARGB(255, 230, 0, 234),
            ),
            borderRadius: const BorderRadius.only(
              topRight: Radius.circular(30),
            ),
          ),
        ),
      ),
    );
  }
}
