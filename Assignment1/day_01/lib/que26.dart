// Create a screen that displays as asset image of the food item at the top of the screen, below the image, display the name of the food item and below the name give thr description of the item . Add appropriate padding.

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class Que26 extends StatefulWidget {
  const Que26({super.key});

  @override
  State<Que26> createState() => _Que26State();
}

class _Que26State extends State<Que26> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blue,
        title: const Text(
          "Daily Flash",
          style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 20,
          ),
        ),
      ),
      body: Container(
        padding: const EdgeInsets.only(
            // top: 5,
            ),
        width: 400,
        // height: 300,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Image.asset(
              width: 400,
              height: 300,
              "assets/images/vadapav.jpg",
            ),
            Container(
              padding: const EdgeInsets.only(
                left: 20,
              ),
              child: const Text(
                "Vada Pav",
                style: TextStyle(
                  fontWeight: FontWeight.w500,
                  fontSize: 25,
                  color: Colors.black,
                ),
              ),
            ),
            Container(
              padding: const EdgeInsets.only(
                left: 20,
                right: 8,
                top: 5,
              ),
              child: const Text(
                '''Vada Pav is a popular street food in India, particularly in the state of Maharashtra. It originated in Mumbai and has since become a beloved snack across the country. The dish consists of a spicy and flavorful deep-fried potato patty, known as the "vada," sandwiched between a pav, which is a soft and square-shaped bun''',
              ),
            ),
          ],
        ),
      ),
    );
  }
}
