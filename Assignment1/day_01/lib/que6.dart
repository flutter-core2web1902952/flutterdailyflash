// Create a screen that displays the container in the center having size (height:200,width:200).The container must have boder with rounded edges .The border must be of the color red .Display a ttext in the center.

import 'package:flutter/material.dart';

class Que6 extends StatefulWidget {
  const Que6({super.key});

  @override
  State<Que6> createState() => _Que6State();
}

class _Que6State extends State<Que6> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: const Color.fromRGBO(95, 63, 4, 0.996),
        title: const Text(
          "AppBar",
          style: TextStyle(
            fontSize: 15,
            fontWeight: FontWeight.w800,
            color: Colors.white,
          ),
        ),
      ),
      body: Center(
        child: Container(
          width: 200,
          height: 200,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(30),
            border: Border.all(
              color: Colors.red,
            ),
          ),
          child: const Center(
            child: Text("Container"),
          ),
        ),
      ),
    );
  }
}
