// Create an Elevated Button in the center of the screen.The button must have rounded edges.Give a shadow of color red to button.

import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'package:google_fonts/google_fonts.dart';

class Que16 extends StatefulWidget {
  const Que16({super.key});

  @override
  State<Que16> createState() => _Que16State();
}

class _Que16State extends State<Que16> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: ElevatedButton(
          style: ElevatedButton.styleFrom(
            shadowColor: Colors.red,
            elevation: 10,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(30),
            ),
          ),
          onPressed: () {},
          child: Text(
            "Click",
            style: GoogleFonts.quicksand(
              fontWeight: FontWeight.w800,
            ),
          ),
        ),
      ),
    );
  }
}
