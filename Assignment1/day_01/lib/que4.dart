import 'package:flutter/material.dart';

class Que4 extends StatefulWidget {
  const Que4({super.key});

  @override
  State<Que4> createState() => _Que4State();
}

class _Que4State extends State<Que4> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: const Color.fromARGB(234, 87, 41, 1),
        title: const Text(
          "AppBar",
          style: TextStyle(
            fontWeight: FontWeight.w800,
            fontSize: 15,
            color: Colors.white,
          ),
        ),
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(
            bottom: Radius.circular(30),
          ),
        ),
      ),
      body: Center(
        child: Container(
          width: 300,
          height: 300,
          decoration: BoxDecoration(
            color: Colors.blue,
            border: Border.all(
              color: Colors.red,
            ),
          ),
        ),
      ),
    );
  }
}
