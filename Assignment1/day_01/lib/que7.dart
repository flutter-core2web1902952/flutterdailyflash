// In the screen add a container of size(width:100,heigth:100) thaat msut only have a left border of width 5 and color as per your choice.Give padding to the container and display a text in the container.

import 'package:flutter/material.dart';

class Que7 extends StatefulWidget {
  const Que7({super.key});

  @override
  State<Que7> createState() => _Que7State();
}

class _Que7State extends State<Que7> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: const Color.fromRGBO(95, 63, 4, 0.996),
        title: const Text(
          "Assignment 2",
          style: TextStyle(
            fontWeight: FontWeight.w800,
            fontSize: 15,
            color: Colors.white,
          ),
        ),
      ),
      body: Center(
        child: Container(
          padding: const EdgeInsets.all(8.0),
          width: 100,
          height: 100,
          decoration: const BoxDecoration(
            color: Colors.green,
            border: Border(
              left: BorderSide(
                color: Colors.red,
                width: 5.0,
              ),
            ),
          ),
          child: const Center(
            child: Text("Container"),
          ),
        ),
      ),
    );
  }
}
