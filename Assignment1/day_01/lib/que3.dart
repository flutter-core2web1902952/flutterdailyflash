// Create s screen that will display an appbar.Add a title in the AppBar the app bar must have a round rectangular border at the bottom.

import 'package:flutter/material.dart';

class Que3 extends StatefulWidget {
  const Que3({super.key});

  @override
  State<Que3> createState() => _Que3State();
}

class _Que3State extends State<Que3> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: const Color.fromARGB(255, 73, 32, 2),
        title: const Text("AppBar"),
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(
            bottom: Radius.circular(30),
          ),
        ),
      ),
    );
  }
}
