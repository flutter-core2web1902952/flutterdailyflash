// Create a screen in which we have 3 container in a cloumn each container must be of heoght 100 and width 100. Each container must have an image as a child.

import 'package:flutter/material.dart';

class Que22 extends StatefulWidget {
  const Que22({super.key});

  @override
  State<Que22> createState() => _Que22State();
}

class _Que22State extends State<Que22> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color.fromARGB(255, 229, 169, 41),
      body: SizedBox(
        width: 360,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SizedBox(
              width: 100,
              height: 100,
              child: Image.asset(
                "assets/images/que22img1.jpeg",
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            SizedBox(
              width: 100,
              height: 100,
              child: Image.asset(
                "assets/images/que22img2.jpeg",
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            SizedBox(
              width: 100,
              height: 100,
              child: Image.asset(
                "assets/images/que22img3.jpeg",
              ),
            ),
          ],
        ),
      ),
    );
  }
}
